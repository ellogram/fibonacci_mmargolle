package eu.ensg.mmargolle.fibonacci.core;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigInteger;
import eu.ensg.mmargolle.fibonacci.core.FibonacciSequence;

public class TestFibonacciSequence {

    @Test
    public void testGetReturnsZero() {
        assertEquals(BigInteger.ZERO, FibonacciSequence.get(0));
        assertEquals(BigInteger.ZERO, FibonacciSequence.get(-1));
        assertEquals(BigInteger.ZERO, FibonacciSequence.get(Long.MIN_VALUE));
    }
    
    @Test
    public void testGetReturnsOne() {
        assertEquals(BigInteger.ONE, FibonacciSequence.get(1));
    }
    
    @Test
    public void testGetReturnsValue() {
        assertEquals(BigInteger.valueOf(233L), FibonacciSequence.get(13));
    }

}
