package eu.ensg.mmargolle.fibonacci.webapp;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import eu.ensg.mmargolle.fibonacci.core.FibonacciSequence;

/**
 * REST API for computing elements of the Fibonacci Sequence
 */
@RestController
public class FibonacciController {

    /**
     * Computes an element of the Fibonacci Sequence given its rank
     * @param rank The rank of the element to compute
     * @return The value for the given rank (as a string for correct precision)
     */
    @GetMapping("/fibonacci/{rank}")
    public String fibonacci(@PathVariable long rank) {
        return String.valueOf(FibonacciSequence.get(rank));
    }

}
