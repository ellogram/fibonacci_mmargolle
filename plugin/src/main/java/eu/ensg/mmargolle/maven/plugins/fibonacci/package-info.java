/**
 * Maven plugin for computing elements of the Fibonacci Sequence
 */
package eu.ensg.mmargolle.maven.plugins.fibonacci;