# Build JAVA applications using Apache Maven (http://maven.apache.org)
# This template will build and test your projects as well as create the documentation.

variables:
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true -Djdk.net.URLClassPath.disableClassPathURLCheck=true"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_NAME"'
cache:
  paths:
    - .m2/repository

stages:
  - build
  - test
  - staging
  - deploy


#### Build stage ####

# This will only validate and compile stuff and run e.g. maven-enforcer-plugin.
# Because some enforcer rules might check dependency convergence and class duplications
# we use `test-compile` here instead of `validate`, so the correct classpath is picked up.
.validate: &validate
  stage: build
  script:
    - 'mvn $MAVEN_CLI_OPTS test-compile'

# Validate merge requests using JDK8
validate:jdk8:
  <<: *validate
  image: maven:3.6.0-jdk-8

# Validate merge requests using JDK11
validate:jdk11:
  <<: *validate
  except: 
    - master
  image: maven:3.6.0-jdk-11


#### Test stage ####

# For allowing merge requests run `verify`.
.verify: &verify
  stage: test
  script:
    - 'mvn $MAVEN_CLI_OPTS verify'
    - awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print covered, "/", instructions, "instructions covered"; print 100*covered/instructions, "% covered" }' core/target/site/jacoco/jacoco.csv
  artifacts:
    reports:
      junit: "*/target/*/TEST-*.xml"
    paths:
      - core/target/jacoco.exec
      - core/target/*.jar
      - plugin/target/*.jar
      - webapp/target/*.jar

# Verify merge requests using JDK8
verify:jdk8:
  <<: *verify
  image: maven:3.6.0-jdk-8

# Verify merge requests using JDK11
verify:jdk11:
  <<: *verify
  except:
    - master
  image: maven:3.6.0-jdk-11


#### Staging stage ####

# For master & dev, create maven site
.pre_deploy: &pre_deploy
  stage: staging
  only:
    - master
    - dev
  dependencies:
    - verify:jdk8
  image: maven:3.6.0-jdk-8

pre_deploy:master:
  <<: *pre_deploy
  script:
    - 'git checkout master'
    - 'mvn $MAVEN_CLI_OPTS site site:stage'
    - 'mv target/staging master'
  
  # Archive up the built documentation site.
  artifacts:
    paths:
      - master

pre_deploy:dev:
  <<: *pre_deploy
  script:
    - 'git checkout dev'
    - 'mvn $MAVEN_CLI_OPTS site site:stage'
    - 'mv target/staging dev'
  # Archive up the built documentation site.
  artifacts:
    paths:
      - dev


#### Deploy stage ####

pages:
  stage: deploy
  only:
    - master
    - dev
  variables:
    GIT_STRATEGY: none
  # BusyBox combines tiny versions of many common UNIX utilities into a single small executable
  image: busybox:latest
  dependencies:
    - pre_deploy:master
    - pre_deploy:dev
  script:
    - 'mkdir -p public/master'
    - 'mkdir -p public/dev'
    - 'mv master/* public/master'
    - 'mv dev/* public/dev'
  artifacts:
    paths:
      - public
